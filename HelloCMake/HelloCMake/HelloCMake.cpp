// HelloCMake.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

int main()
{
    std::cout << "Hejsan Kalle Svensson!\n"; 

	float startkm; 
	float stopkm; 
	float resultat; 

	float korstracka;

	float tankning1; 
	float bensinforbrukning; 

	float bensinkostnad;
	float korkostnad;

	std::cout << "Vad st\x86r din m\x84tarst\x84llning p\x86 just nu n\x84r du ska b\x94rjar k\x94ra [km]?"; 
	std::cin >> startkm;

	std::cout << "Vad st\x86r din m\x84tarst\x84llning p\x86 nu n\x84r du kommit fram [km]?";
	std::cin >> stopkm;
    resultat = stopkm - startkm;
    korstracka = resultat / 10; 
	std::cout << "Du har k\x94rt " << korstracka << " mil sedan du tankade sist" << std::endl;

	std::cout << "Hur m\x86nga liter har du tankat?";
	std::cin >> tankning1;

	bensinforbrukning = tankning1 / korstracka;
	std::cout.precision(2);

	std::cout << std::fixed << "Din bensinf\x94rbrukning \x84r " << bensinforbrukning << " liter per mil" << std::endl;

	std::cout << "Hur mycket kostade bensinen per liter? ";
    std::cin >> bensinkostnad;

	korkostnad = bensinkostnad * bensinforbrukning;
	std::cout.precision(2);

	std::cout << std::fixed << "Din resa har kostat dig " << korkostnad << " kr / mil";

}


